import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ProductosService } from '../../servicios/productos.service';



@Component({
  selector: 'app-producto',
  templateUrl: './producto.component.html',
  styles: []
})
export class ProductoComponent  {

  producto: any = {};

  constructor(private avtivatedRoute: ActivatedRoute,
              private _productosService: ProductosService
  ) {
    this.avtivatedRoute.params.subscribe(params => {
      console.log(params['id']);
      this.producto = this._productosService.getProducto(params['id']);
      console.log(this.producto);
    });
  }


}
