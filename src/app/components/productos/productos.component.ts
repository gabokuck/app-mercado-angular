import { Component, OnInit } from '@angular/core';
import { ProductosService, Producto } from '../../servicios/productos.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-productos',
  templateUrl: './productos.component.html',
  styles: []
})
export class ProductosComponent implements OnInit {

  productos: Producto[] = [];

  constructor(private _productosServise: ProductosService,
              private router: Router
  ) { }

  ngOnInit() {
    this.productos = this._productosServise.getProductos();
    console.log(this.productos);
  }

  verProducto ( idx: number ) {
    this.router.navigate(['/producto', idx]);
  }

}
