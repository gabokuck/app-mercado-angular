import { Injectable } from '@angular/core';

@Injectable()
export class ProductosService {

    private productos: Producto[] = [
        {
            producto: 'Pantalon',
            img: 'assets/img/img1.png',
            descripcion: 'Pantalon cafe',
            precio: '$200',
            marca: 'Levis'
        },
        {
            producto: 'Camisa',
            img: 'assets/img/img2.png',
            descripcion: 'Camisa verda',
            precio: '$250',
            marca: 'Polo'
        },
        {
            producto: 'Reloj',
            img: 'assets/img/img3.png',
            descripcion: 'Reloj resistente al agua',
            precio: '$500',
            marca: 'Armany'
        },
        {
            producto: 'Pantalon',
            img: 'assets/img/img4.jpg',
            descripcion: 'Pantalon cafe',
            precio: '$200',
            marca: 'Levis'
        },
        {
            producto: 'Pantalon',
            img: 'assets/img/img5.jpg',
            descripcion: 'Pantalon cafe',
            precio: '$200',
            marca: 'Levis'
        },
        {
            producto: 'Pantalon',
            img: 'assets/img/producto.png',
            descripcion: 'Pantalon cafe',
            precio: '$200',
            marca: 'Levis'
        },
        {
            producto: 'Pantalon',
            img: 'assets/img/producto.png',
            descripcion: 'Pantalon cafe',
            precio: '$200',
            marca: 'Levis'
        },
        {
            producto: 'Pantalon',
            img: 'assets/img/producto.png',
            descripcion: 'Pantalon cafe',
            precio: '$200',
            marca: 'Levis'
        },
        {
            producto: 'Pantalon',
            img: 'assets/img/producto.png',
            descripcion: 'Pantalon cafe',
            precio: '$200',
            marca: 'Levis'
        },
        {
            producto: 'Pantalon',
            img: 'assets/img/producto.png',
            descripcion: 'Pantalon cafe',
            precio: '$200',
            marca: 'Levis'
        },
        {
            producto: 'Pantalon',
            img: 'assets/img/producto.png',
            descripcion: 'Pantalon cafe',
            precio: '$200',
            marca: 'Levis'
        }
    ];

    constructor() {
        console.log('Servicio listo para usarse');
    }

    getProductos(): Producto[] {
        return this.productos;
    }

    getProducto( idx: string) {
        return this.productos[idx];
    }
}

export interface Producto {
    producto: String;
    img: String;
    descripcion: String;
    precio: String;
    marca: String;
}
